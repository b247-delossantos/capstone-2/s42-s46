// cart routes
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");

// route to add an item to the cart
router.post("/add", (req, res) => {
const data = {
productId: req.body.productId,
productName: req.body.productName,
price: req.body.price,
quantity: req.body.quantity
};
const cartItems = cartController.addToCart(data);
res.send(cartItems);
});

// route to remove an item from the cart
router.delete("/remove/:productId", (req, res) => {
const productId = req.params.productId;
const cartItems = cartController.removeFromCart(productId);
res.send(cartItems);
});

// route to update the quantity of an item in the cart
router.put("/update", (req, res) => {
const data = {
productId: req.body.productId,
quantity: req.body.quantity
};
const cartItems = cartController.updateCartQuantity(data);
res.send(cartItems);
});

// route to get the subtotal for an item in the cart
router.get("/:productId/subtotal", (req, res) => {
const productId = req.params.productId;
const subtotal = cartController.calculateSubtotal(productId);
res.send({ subtotal });
});

// route to get the total price for all items in the cart
router.get("/total", (req, res) => {
const total = cartController.calculateTotal();
res.send({ total });
});

module.exports = router;