const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

const auth = require("../auth");

//create order

router.post("/checkout", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    productId: req.body.products[0].productId,
    quantity: req.body.products[0].quantity,
    totalAmount: req.body.totalAmount,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  console.log(data); // log the request data to the console

  orderController.createOrder(data).then((resultFromController) => res.send(resultFromController));
});



//retrieve all orders
router.get("/all", (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});




//Retrieve authenticated User's Orders
router.get("/details", auth.verify, (req, res) => {
 const data =  {userId : auth.decode(req.headers.authorization).id};
    
  console.log(data);

  orderController.getUserOrders(data).then(resultFromController => {
    res.send(resultFromController);
  });
});




module.exports = router;
