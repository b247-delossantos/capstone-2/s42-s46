const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// Route for create a product
router.post("/addproduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all", (req, res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/active", (req, res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

//Route for retrieving Single Product

router.get("/:productId/details", (req, res) => {


	console.log(req.params.productId);

	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product

router.put("/:productId/update", auth.verify, (req, res) => {


	const data = {
		productName: req.body.productName,
		description: req.body.description,
		price: req.body.price,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.isAdmin);
	productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
})
// Route for archiving a product

router.put("/:productId/archive", auth.verify, (req, res) => {

		const data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
	productController.archiveProduct(data, req.params).then(resultFromController => res.send(resultFromController));
})



// Route for activating a product

router.put("/:productId/activate", auth.verify, (req, res) => {
	
	const data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

	productController.activateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;