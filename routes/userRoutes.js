const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");




// Check the email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

//Register a user
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for setting a user as an admin
router.put("/make-admin", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  userController.makeAdmin(data, req.body).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
