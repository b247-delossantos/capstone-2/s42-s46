const Order = require("../models/Order");

module.exports.createOrder = (data) => {
  if (data.isAdmin === false) {
    const newOrder = new Order({
      userId: data.id,
      products: data.products,
      totalAmount: data.totalAmount,
    });

  	 return newOrder.save().then((product, error) => {
			if(error){
				return {
					message: "Unable to create a order"
				}
			} else {
				return {
					message: "New order successfully created!"
				}
			};
		});

	} 

	let message = Promise.resolve({
		message: "User must be an non-addmin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};

//Retrieve all orders
module.exports.getAllOrders = (data) => {

if(data.isAdmin === true){
	return Order.find({}).then(result => {
		return result
	});

	
};
let message = Promise.resolve({
		message: "User must be an non-addmin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};


//Retrieving User's Orders

module.exports.getUserOrders = (data) => {
	
	
  return Order.findById(data.userId).then(result => {
    	 console.log(result);
      return result;
    });

};