const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");



module.exports.checkEmailExists = (reqBody) => {
	console.log(reqBody);

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


//register a user
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return {
				message: "Unable to register"
			}
		} else {
			return {
				message: "You have successfully registered!"
			}
		};
	});
};

//authenticate a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return false;
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				
				return { access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

//Setting a user as admin
module.exports.makeAdmin = (data, reqBody) => {
  if (data.isAdmin === true) {
    const { userId, isAdmin } = reqBody;

    const updateAdminField = {
      isAdmin: isAdmin
    };

    return User.findByIdAndUpdate(userId, updateAdminField)
      .then((user, error) => {
        if (error) {
          return { message: "Error updating user" };
        } else {
          return { message: "User has been updated" };
        }
      });
  } else {
    return Promise.resolve({ message: "User must be an admin to access this!" });
  }
};

//Retrieving User Details
module.exports.getProfile = (data) => {
	
	return User.findById(data.userId).then(result => {
		console.log(result);
		
		// Changes the value of the user's password to an empty string when returned to the frontend
		result.password = "";
		return result;
	});
	
};
