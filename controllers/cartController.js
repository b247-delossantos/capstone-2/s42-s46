const Cart = require("../models/Cart");
const User = require("../models/User");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

// cart controller
let cartItems = [];

// function to add an item to the cart
module.exports.addToCart = (data) => {
const itemIndex = cartItems.findIndex(item => item.productId === data.productId);
if (itemIndex !== -1) {
cartItems[itemIndex].quantity += data.quantity;
} else {
cartItems.push(data);
}
return cartItems;
};

// function to remove an item from the cart
module.exports.removeFromCart = (productId) => {
const itemIndex = cartItems.findIndex(item => item.productId === productId);
if (itemIndex !== -1) {
cartItems.splice(itemIndex, 1);
}
return cartItems;
};

// function to update the quantity of an item in the cart
module.exports.updateCartQuantity = (data) => {
const itemIndex = cartItems.findIndex(item => item.productId === data.productId);
if (itemIndex !== -1) {
cartItems[itemIndex].quantity = data.quantity;
}
return cartItems;
};

// function to calculate the subtotal for each item in the cart
module.exports.calculateSubtotal = (productId) => {
const item = cartItems.find(item => item.productId === productId);
if (item) {
return item.price * item.quantity;
} else {
return 0;
}
};

// function to calculate the total price for all items in the cart
module.exports.calculateTotal = () => {
let total = 0;
cartItems.forEach(item => {
total += item.price * item.quantity;
});
return total;
};