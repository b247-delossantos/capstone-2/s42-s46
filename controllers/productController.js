const Product = require("../models/Product");



// create a product
module.exports.createProduct = (data) => {
	console.log(data);
	if(data.isAdmin === true){

		let newProduct = new Product({
			productName : data.product.productName,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {
			if(error){
				return {
					message: "Unable to create a product"
				}
			} else {
				return {
					message: "New product successfully created!"
				}
			};
		});

	} 

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};

//Get all products

module.exports.getAllProduct = () => {

	return Product.find({}).then(result => {
		return result
	});
};

// Get all active products

module.exports.getAllActiveProducts = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Get specific product
module.exports.getSingleProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

// Update a product

module.exports.updateProduct = (data, reqParams) => {
	console.log(data.isAdmin);

	if(data.isAdmin === true){
		let updatedProduct = {
		productName : data.productName,
		description : data.description,
		price : data.price
	};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false
			} else {
				return {
					message: "New product successfully created!"
				}
			};
		});

	} 

	

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};
	

// Archive a product
module.exports.archiveProduct = (data, reqParams) => {


if(data.isAdmin === true){
		let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else{
			return true;
		};
	});

	} 

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};

	


// activate a product

module.exports.activateProduct = (data, reqParams) => {

	if(data.isAdmin === true){
		let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else{
			return true;
		};
	});

	} 

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};